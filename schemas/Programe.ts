import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Programe = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true,label:"لقب" }),
        heading: text({label:"عنوان"}),
        description:text({label:"وصف"}),
        author: relationship({
          ref: 'User.program',
          label:"ضع الكلمة المناسبة"
        }),
        policy:relationship({ref:"Policy.policy",label:"بوليصة"}),
        guidebook:relationship({ref:"Guide.guide",label:"كتاب دليل"}),
        admissionsupport:relationship({ref:"Support.admissionsupport",label:"دعم القبول"}),
        admissiondate:relationship({ref:"Date.admissiondate",label:"تاريخ القبول",many:true}),

        publishDate: timestamp({label:"تاريخ النشر"}),
        picture:image({label:"صورة"})
      }
});