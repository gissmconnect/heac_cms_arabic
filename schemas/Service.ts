import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Service = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true, label:"لقب"}),
        link: text({label:" حلقة الوصل"}),
        description:text({label:"وصف"}),
        linkservice:relationship({ref:'ElectronicService.serviceLinks',label:"خدمة الارتباط"}),
        publishDate: timestamp({label:"تاريخ النشر"}),
        picture:image({label:"صورة"}),
        
      }
});