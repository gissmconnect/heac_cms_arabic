import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Gallery = list({
    fields: {
        title: text({label:"لقب"}),
        author: relationship({
          ref: 'User.gallery',
          label:"مؤلف"
        }),
        publishDate: timestamp({label:"تاريخ النشر"}),
        picture: image({ 
            isRequired: true,
            label:"صورة"
        }),
        image: relationship({
          ref:"Heacnew.picture",
          label:"علاقة الصورة"
        })

      }
});