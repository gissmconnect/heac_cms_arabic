import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image,file } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const UpFooter = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true,label:"لقب" }),
        link1: text({label:"1 حلقة الوصل"}),
        link2: text({label:"2 حلقة الوصل"}),
        description:text({label:"وصف"}),
        publishDate: timestamp({label:"تاريخ النشر"}),
        icon1:image({label:"أيقونة1"}),
        icon2:image({label:"2أيقونة"}),
        
      }
});