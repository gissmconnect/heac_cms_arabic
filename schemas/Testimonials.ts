import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image, checkbox } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Testimonial = list({
    access: {
      create: permissions.canManagePeople,
      read: true,
      update: rules.canUpdatePeople,
      delete: permissions.canManagePeople,
    },
    fields: {
        name: text({ isRequired: true,label:"اسم" }),
        description: text({label:"وصف"}),
        designation: text({label:"تعيين"}),
        publishDate: timestamp({label:"تاريخ النشر"}),
        picture:image({label:"صورة"}),
        createdAt:timestamp({defaultValue:new Date(),label:"أنشئت في"}),
        isPublish:checkbox({
            defaultValue:false,
            label:"هو النشر"
        })
        
      }
});