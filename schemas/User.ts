import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const User = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      ui: {
        hideCreate: args => !permissions.canManagePeople(args),
        hideDelete: args => !permissions.canManagePeople(args),
        listView: {
          initialColumns: ['name', 'posts', 'role'],
        },
        itemView: {
          defaultFieldMode: ({ session, item }) => {
            // People with canEditOtherPeople can always edit people
            if (session.data.role?.canEditOtherPeople) return 'edit';
            // People can also always edit themselves
            else if (session.itemId === item.id) return 'edit';
            // Otherwise, default all fields to read mode
            return 'read';
          },
        },
      },
      fields: {
        name: text({ isRequired: true }),
        email: text({ isRequired: true, isUnique: true }),
        password: password({ 
          isRequired: true,
          access: {
            update: ({ session, item }) =>
              permissions.canManagePeople({ session }) || session.itemId === item.id,
          },
        }),
        /* The role assigned to the user */
        role: relationship({
          ref: 'Role.assignedTo',
          access: {
            create: permissions.canManagePeople,
            update: permissions.canManagePeople,
          },
          ui: {
            itemView: {
              fieldMode: args => (permissions.canManagePeople(args) ? 'edit' : 'read'),
            },
          },
        }),
        // posts: relationship({ ref: 'Post.author', many: true }),
        news: relationship({ ref: 'Heacnew.author', many: true }),
        slide: relationship({ ref: 'Slider.author', many: true }),
        gallery: relationship({ ref: 'Gallery.author', many: true }),
        announcement: relationship({ ref: 'Announcement.author', many: true }),
        program:relationship({ref:"Programe.author",label:"برنامج"})
      },
});