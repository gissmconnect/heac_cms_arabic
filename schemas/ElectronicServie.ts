import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image,checkbox } from '@keystone-next/fields';
import { permissions, rules } from '../access';
import { document } from '@keystone-next/fields-document';

export const ElectronicService = list({
    access: {
      create: permissions.canManagePeople,
      read: true,
      update: rules.canUpdatePeople,
      delete: permissions.canManagePeople,
    },
    fields: {
      // content: document({
      //   formatting: true,
      //   dividers: true,
      //   links: true,
      //   layouts: [
      //     [1, 1],
      //     [1, 1, 1],
      //   ],
      // }),
        title: text({ isRequired: true,label:"لقب" }),
        description: document({
          formatting: true,
          dividers: true,
          links: true,
          layouts: [
            [1, 1],
            [1, 1, 1],
          ],label:"وصف"
        }),
        serviceLinks: relationship({
          ref:"Service.linkservice",
          many:true,
          label:"روابط الخدمة"
        }),
        icon:image({label:"أيقونة"}),
        eImages: image({label:"صورة"}),
        publishDate: timestamp({label:"تاريخ النشر"}),
        createdAt:timestamp({defaultValue:new Date(),label:"أنشئت في"}),
        isPublish:checkbox({
            defaultValue:false,
            label:"يتم نشره"
        })
      }
});