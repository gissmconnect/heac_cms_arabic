import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image,file } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Guide = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true,label:"لقب" }),
        link: text({label:"حلقة الوصل"}),
        description:text({label:"وصف"}),
        publishDate: timestamp({label:"تاريخ النشر"}),
        picture:image({label:"صورة"}),
        icon:image({label:"أيقونة"}),
        pdfbook:file({label:"كتاب pdf"}),
        guide:relationship({ref:"Programe.guidebook",label:"يرشد"})
      }
});