import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image,file } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Date = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        programeCode: text({ isRequired: true,label:"كود البرنامج" }),
        link: text({label:"حلقة الوصل"}),
        description:text({label:"وصف"}),
        startDate:timestamp({isRequired:true,label:"تاريخ البدء"}),
        endDate:timestamp({isRequired:true,label:"تاريخ الانتهاء"}),
        publishDate: timestamp({label:"تاريخ النشر"}),
        picture:image({label:"صورة"}),
        icon:image({label:"أيقونة"}),
        admissiondate:relationship({ref:"Programe.admissiondate",label:"تاريخ القبول"})
      }
});