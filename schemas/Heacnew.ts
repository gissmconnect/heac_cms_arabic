import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Heacnew = list({
    access: {
      create: permissions.canManagePeople,
      read: true,
      update: rules.canUpdatePeople,
      delete: permissions.canManagePeople,
    },
    fields: {
        title: text({ isRequired: true,label:"لقب" }),
        description: text({label:"وصف"}),
        author: relationship({
          ref: 'User.news',
          label:"مؤلف"
        }),
        publishDate: timestamp({label:"تاريخ النشر"}),
        picture: relationship({
          ref:"Gallery.image",
          many:true
        })
        
      }
});