import { config, createSchema } from '@keystone-next/keystone/schema';
import { statelessSessions } from '@keystone-next/keystone/session';
import { createAuth } from '@keystone-next/auth';
require('dotenv').config();
import { isSignedIn } from './access';
import { User } from './schemas/User';
import { Post } from './schemas/Post';
import { Tag } from './schemas/Tag';
import { Heacnew } from './schemas/Heacnew';
import { Slider } from './schemas/Slider';
import { Gallery } from './schemas/Gallery';
import { Announcement } from './schemas/Announcement';
import { Role } from './schemas/Role';
import { ElectronicService } from './schemas/ElectronicServie';
import {Testimonial} from './schemas/Testimonials';
import {Service} from './schemas/Service';
import {Sponsor} from './schemas/Sponsor';
import {Date} from './schemas/Date';
import {Guide} from './schemas/Guide';
import {Support} from './schemas/Support';
import {Programe} from './schemas/Programe';
import {UpFooter} from './schemas/UpFooter';

import {Policy} from './schemas/Policy';

let sessionSecret = process.env.SESSION_SECRET;

if (!sessionSecret) {
  if (process.env.NODE_ENV === 'production') {
    throw new Error(
      'The SESSION_SECRET environment variable must be set in production'
    );
  } else {
    console.log(process.env.DATABASE_URL,"*********")
    sessionSecret = '-- DEV COOKIE SECRET; CHANGE ME --';
  }
}

let sessionMaxAge = 60 * 60 * 24 * 30; // 30 days

const { withAuth } = createAuth({
  listKey: 'User',
  identityField: 'email',
  secretField: 'password',
  // sessionData: 'name',
  initFirstItem: {
    fields: ['name', 'email', 'password'],
    itemData: {
      /*
        This creates a related role with full permissions, so that when the first user signs in
        they have complete access to the system (without this, you couldn't do anything)
      */
      role: {
        create: {
          name: 'Admin Role',
          canCreateTodos: true,
          canManageAllTodos: true,
          canSeeOtherPeople: true,
          canEditOtherPeople: true,
          canManagePeople: true,
          canManageRoles: true,
        },
      },
    },
  },
  /* This loads the related role for the current user, including all permissions */
  sessionData: `
  name role {
    id
    name
    canCreateTodos
    canManageAllTodos
    canSeeOtherPeople
    canEditOtherPeople
    canManagePeople
    canManageRoles
  }`,
});

const session = statelessSessions({
  maxAge: sessionMaxAge,
  secret: sessionSecret,
});

export default withAuth(
  config({
    db: {
      adapter: 'prisma_postgresql',
      url: process.env.DATABASE_URL || 'postgres://user:123@localhost/keystone-project',
    },
    ui: {
      // isAccessAllowed: (context) => !!context.session?.data,
      /* Everyone who is signed in can access the Admin UI */
      isAccessAllowed: isSignedIn,
    },
    server: {
      cors: { origin: ['*'], credentials: true },
      port: 3000,
      maxFileSize: 200 * 1024 * 1024
    },
    images: {
      upload: 'local',
      local: {
        storagePath: 'public/images',
        baseUrl: '/images',
      },
    },
    files: {
      upload: 'local',
      local: {
        storagePath: '/var/www/files',
        baseUrl: '/files',
      },
    },
    lists: createSchema({
      User,
      // Post,
      // Tag,
      Heacnew,
      Slider,
      Gallery,
      Announcement,
      Role,
      ElectronicService,
      Testimonial,
      Service,
      Sponsor,
      Date,
      Guide,
      Support,
      Programe,
      Policy
    }),
    session,
  })
);